import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./pages/login";
import UserProfile from "./pages/userProfile";
import Users from "./pages/users";
import Clusters from "./pages/clusters";
import ClusterDetails from "./pages/clusterDetails";
import UserDetails from "./pages/userDetails";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route index element={<Login />} />
          <Route path="/" element={<Login />} />
          <Route path="/login" element={<Login />} />
          <Route path="/user" element={<UserProfile />} />
          <Route path="/users" element={<Users />} />
          <Route path="/userdetails" element={<UserDetails />} />
          <Route path="/clusters" element={<Clusters />} />
          <Route path="/cluster" element={<ClusterDetails />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
