import React from "react";
import { Link } from "react-router-dom";

const BackButton = (data) => {
  return(<Link title="Back" className="links" to={`/user?id=${data.user.uid}`}><span className="fas fa-long-arrow-alt-left"> <i> Back to profile</i></span></Link>)
}
export default BackButton;