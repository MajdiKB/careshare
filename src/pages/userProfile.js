import React, { useEffect, useState } from "react";
import { getAuth } from "../lib/firebase";
import Navbar from "./navbar";
import Logout from "./logout";
import { getUserDetails } from "../lib/db";

const UserProfile = () => {
  const [userData, setUserData] = useState();
  const [user, setUser] = useState();

  useEffect(() => {
    getUser();
  }, []);

  const getUser = async () => {
    setUser(getAuth().currentUser);
    let userDataTemp = getAuth().currentUser;
    if (userDataTemp) {
      userDataTemp = await getUserDetails(userDataTemp.uid);
      setUserData(userDataTemp);
    }
  };
  // console.log('User logged: ', userData);
  return (
    <div>
      {user && userData ? (
        <div>
          <Logout user={user} />
          <Navbar />
          <div className="section-container">
            <span>{user.displayName ? user.displayName : user.email}</span>
            <span>{userData.userType === "1" ? "Admin" : "User"}</span>
          </div>
        </div>
      ) : (
        <h3 className="container">loading data...</h3>
      )}
    </div>
  );
};

export default UserProfile;
