import React from "react";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import BackButton from "../components/backbutton";
import { getAuth } from "../lib/firebase";
import Navbar from "./navbar";
import Logout from "./logout";
import UserDetails from "./userDetails";
import {getUsers, getUserDetails} from "../lib/db";

const Users = () => {
  const [allUsers, setAllUsers] = useState([]);
  const navigate = useNavigate();
  const user = getAuth().currentUser;

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const users = await getUsers();
    if (users){
      setAllUsers(users);
    }
  }

  const getDetails = async (event) => {
    const userId = event.target.value;
    const details = await getUserDetails(userId);
    navigate(`../userdetails?id=${details.id}`, {state:{userDetails:details}})
  }

  return (
    <div>
      {user ? (
        <div>
          <Logout user={user} />
          <Navbar />
          <div className="section-container">
            <h1>Users</h1>
            {allUsers.length > 0 ? (
              <select
                onChange={(event) => getDetails(event)}
                className="selects"
              >
                {allUsers.map((userSelect) => {
                  return (
                    <option key={userSelect.key} value={userSelect.id}>
                      {userSelect.name}
                    </option>
                  );
                })}
              </select>
            ) : (
              <p>
                <i>Cargando Users</i>
              </p>
            )}
          </div>
          <div className="section-container">
            <p>
              <BackButton user={user} />
            </p>
          </div>
        </div>
      ) : (
        <div>
          <h3 className="container">loading data...</h3>
        </div>
      )}
    </div>
  );
};

export default Users;
