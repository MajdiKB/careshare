import React from "react";
import { useState } from "react";
import { Navigate } from "react-router-dom";
import { getAuth, signInWithEmailAndPassword } from "../lib/firebase";
import "../styles/login.css";

const Login = () => {
  const [user, setUser] = useState("");
  const [pass, setPass] = useState("");
  const [userLogged, setUserLogged] = useState("");
  const [error, setError] = useState("");

  const loginUser = (email, password) => {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        setUserLogged(userCredential.user);
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorCode, errorMessage);
        setError("Incorrect User/Pass");
      });
  };

  //enric@dynatech2012.com
  //123456

  const handleChangeUser = (e) => {
    setUser(e.target.value);
    setError("");
  };
  const handleChangePass = (e) => {
    setPass(e.target.value);
    setError("");
  };
  const handleSubmit = (e) => {
    if (user !== "" && pass !== "") {
      loginUser(user, pass);
    } else {
      setError("No user/pass");
    }
    e.preventDefault();
  };

  return (
    <div>
      {!userLogged ? (
        <div className="container">
          <h1>WeShareCare</h1>

          <form onSubmit={handleSubmit}>
            <div className="container">
              <input
                className="input-login"
                type="text"
                value={user}
                onChange={handleChangeUser}
                placeholder="User"
              />

              <input
                className="input-login"
                type="password"
                value={pass}
                onChange={handleChangePass}
                placeholder="Password"
              />
              {error ? <span className="text-error">{error}</span> : null}
              <input className="pointer" type="submit" value="Submit" />
            </div>
          </form>
        </div>
      ) : (
        <Navigate to={`/user?id=${userLogged.uid}`} />
      )}
    </div>
  );
};

export default Login;
