import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import BackButton from "../components/backbutton";
import { getAuth } from "../lib/firebase";
import Logout from "./logout";
import Navbar from "./navbar";
import Specialities from "./specialities";

const ClusterDetails = () => {
  const location = useLocation();
  const data = location.state.clusterDetails;
  const user = getAuth().currentUser;

  // console.log("Cluster Details: ", data);
  // console.log("Cluster Specialities: ", data.specialties);
  // console.log("Cluster integrations: ", data.integrations);
  return (
    <div>
      <Logout user={user} />
      <Navbar />
      <div className="section-container">
        {data && user ? (
          <div>
            <h2>Cluster "{data.name}" Details:</h2>
            <Specialities
                clusterSpecialties={data.specialties}
                specialtieIntegrations={data.integrations}
                clusterId={data.id}
              />
          </div>
        ) : (
          <p>No Cluster Data</p>
        )}
      </div>
      <div className="section-container">
        <p>
          <BackButton user={user} />
        </p>
      </div>
    </div>
  );
};

export default ClusterDetails;
