import React from "react";
import {
  addSpecialty,
  getCollection,
  getDetails,
  getDetailsArray,
} from "../lib/db";
import { useEffect, useState } from "react";
import Integrations from "./integrations";

const Specialities = (data) => {
  //las integraciomes las detectamos bien, falta pintarlas bien
  //las especialidades hay q convertir el array con id a array con nombres.
  let { clusterSpecialties, specialtieIntegrations, clusterId } = data;
  const [clusterSpecialtiesUpdate, setSpecialtiesUpdate] = useState([]);
  const [allSpecialities, setAllSpecialities] = useState([]);
  const [filterSpecialities, setFilterSpecialities] = useState([]);
  const [clusterSpecialitiesName, setClusterSpecialitiesName] = useState([]);
  const [integrationsOfSpecialtie, setIntegrationsOfSpecialtie] = useState([]);
  const [showIntegration, setShowIntegration] = useState(false);
  const [specialtieKey, setSpecialtieKey] = useState();
  const [specialtieName, setSpecialtieName] = useState();

  useEffect(() => {
    if (clusterSpecialtiesUpdate.length > 0) {
      clusterSpecialties = clusterSpecialtiesUpdate;
    }
    if (clusterSpecialties) {
      getDetailsArray("specialty", Object.keys(clusterSpecialties)).then(
        (result) => setClusterSpecialitiesName(result)
      );
    }
    getCollection("specialty").then(async (result) =>
      setAllSpecialities(
        await getDetailsArray("specialty", Object.keys(result))
      )
    );

    if (specialtieIntegrations) {
      setIntegrationsOfSpecialtie(Object.entries(specialtieIntegrations));
    }
  }, [clusterSpecialtiesUpdate]);

  useEffect(() => {
    filterSpecialist();
  }, [allSpecialities]);

  const filterSpecialist = () => {
    let filtered = allSpecialities.filter(
      (x) =>
        !clusterSpecialitiesName.find((y) => y.name === x.name && y.id === x.id)
    );
    setFilterSpecialities(filtered);
  };

  const handleShowIntegrations = (specialtie, key) => {
    setSpecialtieKey(key);
    setSpecialtieName(specialtie);
    setShowIntegration(true);
  };

  const closeIntegrations = () => {
    setShowIntegration(false);
  };

  const addSpecialtyToCluster = async (cluster, specialty) => {
    await addSpecialty(cluster, specialty);
    await getDetails("clusters", cluster).then((result) =>
      setSpecialtiesUpdate(result.specialties)
    );
    console.log('sp añadido');
  };

  return (
    <div>
      <h2>Specialties:</h2>
      <div>
        {clusterSpecialitiesName
          ? clusterSpecialitiesName?.map((specialtie) => {
              return (
                <div key={specialtie.id}>
                  <button
                    className="btn-specialistie"
                    onClick={() =>
                      handleShowIntegrations(specialtie.name, specialtie.id)
                    }
                    value={specialtie.id}
                  >
                    {specialtie.name}
                  </button>
                </div>
              );
            })
          : null}
      </div>
      {filterSpecialities.length > 0 ? (
        <div>
          <h3>Add specialty</h3>
          {filterSpecialities?.map((specialtyFilter, key) => {
            return (
              <div key={key}>
                <button
                  className="btn-specialistie"
                  value={specialtyFilter.id}
                  onClick={() =>
                    addSpecialtyToCluster(clusterId, specialtyFilter.id)
                  }
                >
                  {specialtyFilter.name}
                </button>
              </div>
            );
          })}
        </div>
      ) : null}
      {showIntegration && specialtieIntegrations ? (
        <div>
          <Integrations
            specialtieKey={specialtieKey}
            specialtieName={specialtieName}
            integrations={specialtieIntegrations}
            closeIntegrations={closeIntegrations}
          />{" "}
        </div>
      ) : null}
    </div>
  );
};

export default Specialities;
