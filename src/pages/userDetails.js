import React, { useState, useEffect } from "react";
import { Navigate, useLocation } from "react-router-dom";
import BackButton from "../components/backbutton";
import { getAuth } from "../lib/firebase";
import Logout from "./logout";
import Navbar from "./navbar";
const UserDetails = () => {
  const location = useLocation();
  const [data, setData] = useState();
  const [clustersArray, setClustersArray] = useState();
  const [specialties, setSpecialties] = useState();
  const [name, setName] = useState();
  const [user, setUser] = useState();

  useEffect(() => {
    setUser(getAuth().currentUser);
    setData(location.state.userDetails);
  }, []);

  useEffect(() => {
    if (data) {
      setClustersArray(data.clustersArray);
      setSpecialties(data.specialties);
      setName(data.name);
    }
  }, [data, user]);

  console.log("User Details: ", data);
  return (
    <div>
      {user ? (
        <div>
          <Logout user={user} />
          <Navbar />
        </div>
      ) : null}

      <div className="section-container">
        {user ? (
          <div>
            <h3>User:</h3>
            <p>{name}</p>
            <h3>Clusters:</h3>
            {clustersArray ? (
              clustersArray?.map((cluster, key) => {
                return <p key={key}>{cluster}</p>;
              })
            ) : (
              <p>
                <i>User without clusters</i>
              </p>
            )}
            <h3>Specialties:</h3>
            {specialties ? (
              specialties?.map((specialtie, key) => {
                return <p key={key}>{specialtie}</p>;
              })
            ) : (
              <p>
                <i>User without clusters</i>
              </p>
            )}
          </div>
        ) : (
          <div>
            <p>No User Data</p>
          </div>
        )}
      </div>
      <div className="section-container">
        {user ? (
          <div>
            <BackButton user={user} />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default UserDetails;
