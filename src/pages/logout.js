import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { firebase } from "../lib/firebase";
const Logout = (data) => {
  const user = data.user;
  const navigate = useNavigate();
  const logOut = () => {
    firebase
      .auth()
      .signOut()
      .then(function () {
        console.log("ok logout");
      })
      .catch(function (error) {
        console.log("Error in logout");
      });
    navigate("/");
  };
  return (
    <div className="user-info-container">
      <div className="container-row">
        {user ? (
          user.displayName !== null ? (
            <div className="container-row">
              <h3>
                <Link
                  title="Profile"
                  className="links"
                  to={`/user?id=${user.uid}`}
                >
                  {user.displayName}
                </Link>
              </h3>
              <span
                title="Log Out"
                onClick={logOut}
                className="fas fa-door-open fa-2x pointer"
              ></span>
            </div>
          ) : (
            <div className="container-row">
              <h3>
                <Link
                  className="links"
                  title="Profile"
                  to={`/user?id=${user.uid}`}
                >
                  {user.email}
                </Link>
              </h3>
              <span
                title="Log Out"
                onClick={logOut}
                className="fas fa-door-open fa-2x pointer"
              ></span>
            </div>
          )
        ) : (
          <p>No user logged</p>
        )}
      </div>
    </div>
  );
};
export default Logout;
