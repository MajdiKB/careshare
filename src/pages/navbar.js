import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar-container">
      <Link className="links" to="/users">
      <span title="Users" className="fas fa-users fa-3x"></span>
      </Link>
      <Link className="links" to="/clusters">
      <span title="Clusters" className="fab fa-accusoft fa-3x"></span>
      </Link>
    </div>
  );
};
export default Navbar;
