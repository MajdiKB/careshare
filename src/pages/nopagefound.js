import React from "react";
const NoPageFound = () => {
  return (
    <div>
      <h3>Page not found.</h3>
    </div>
  );
};

export default NoPageFound;
