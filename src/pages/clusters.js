import React from "react";
import { useState, useEffect } from "react";
import { getAuth } from "../lib/firebase";
import Navbar from "./navbar";
import { getCollection, getDetails } from "../lib/db";
import BackButton from "../components/backbutton";
import Logout from "./logout";
import { useNavigate } from "react-router-dom";

const Clusters = () => {
  const [userClusters, setUserClusters] = useState([]);
  const navigate = useNavigate();
  const user = getAuth().currentUser;
  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    let clusters = await getCollection("clusters");
    if (clusters) {
      setUserClusters(Object.values(clusters));
    }
  };

  const getClusterDetails = async (event) => {
    const clusterId = event.target.value;
    const details = await getDetails("clusters", clusterId);
    navigate(`../cluster?id=${details.id}`, {state:{clusterDetails:details}})
  };
  // console.log(allClusters);

  return (
    <div>
      {user ? (
        <div>
          <Logout user={user} />
          <Navbar />
          <div className="section-container">
            <h1>Clusters</h1>
            {userClusters.length > 0 ? (
              <select
                onChange={(event) => getClusterDetails(event)}
                className="selects"
              >
                {userClusters.map((cluster) => {
                  return (
                    <option key={cluster.id} value={cluster.id}>
                      {cluster.name}
                    </option>
                  );
                })}
              </select>
            ) : (
              <p>
                <i>Cargando Clusters</i>
              </p>
            )}
          </div>
          {/* {clusterData ? <ClusterDetails clusterData={clusterData} /> : null} */}
          <div className="section-container">
            <p>
              <BackButton user={user} />
            </p>
          </div>
        </div>
      ) : (
        <div>
          <h3 className="container">loading data...</h3>
        </div>
      )}
    </div>
  );
};

export default Clusters;
