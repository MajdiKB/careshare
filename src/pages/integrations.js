import React, { useEffect } from "react";
import { getDetailsArray } from "../lib/db";
import { useState } from "react";
const Integrations = (data) => {
  const { integrations, specialtieKey, specialtieName, closeIntegrations } =
    data;

  const [arrayResultIntegrationsName, setArrayResultIntegrationsName] =
    useState();

  useEffect(() => {
    getIntegrationsName();
  }, []);

  const getIntegrationsName = (specialtieKey) => {
    if (integrations[specialtieKey]) {
      getDetailsArray(
        "integrations",
        Object.keys(integrations[specialtieKey])
      ).then((result) => setArrayResultIntegrationsName(result));
      return arrayResultIntegrationsName;
    }
  };
  return (
    <div className="btn-integrations">
      <h3>Integrations of specialtie "{specialtieName}"</h3>
      {getIntegrationsName(specialtieKey)?.map((integration) => {
        return (
          <div key={integration.id}>
            <button
              className="btn-integrations"
              value={integration.id}
              key={integration.id}
            >
              {integration.name}
            </button>
          </div>
        );
      })}
      <button className="btn-integrations" onClick={() => closeIntegrations()}>
        Close integrations
      </button>
    </div>
  );
};

export default Integrations;
