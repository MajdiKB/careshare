import { database, firebase } from "./firebase";
import { getDatabase, ref, child, get, set } from "firebase/database";
import { addDoc, doc, getDoc } from "firebase/firestore";

const firestore = firebase.firestore();

//users

export const getUsers = async () => {
  const usersRef = firestore.collection("users");
  const usersSnap = await usersRef.get();
  let users = [];
  let i = 0;
  for (const userDoc of usersSnap.docs) {
    const userName = userDoc.data().name;
    const userId = userDoc.data().id;
    users.push({ key: i, name: userName, id: userId });
    i = i + 1;
  }
  return users;
};

export const getUserDetails = async (userId) => {
  const userRef = doc(database, "users", userId);
  const userSnap = await getDoc(userRef);
  let data;
  if (userSnap.exists()) {
    data = userSnap.data();
  } else {
    console.log("No data available");
  }
  if (data) {
    return data;
  }
};

//general

export const getCollection = async (collection) => {
  const collectionRef = firebase.database().ref(`${collection}`);
  const snapshot = await collectionRef.once("value");
  return snapshot.val();
};

export const getDetails = async (collection, id) => {
  const dbRef = ref(getDatabase());
  let data;
  await get(child(dbRef, `${collection}/${id}`))
    .then((snapshot) => {
      if (snapshot.exists()) {
        data = snapshot.val();
      } else {
        console.log("No data available");
      }
    })
    .catch((error) => {
      console.error(error);
    });
  if (data) {
    return data;
  }
};

export const getDetailsArray = async (collection, array) => {
  const dbRef = ref(getDatabase());

  let result = await array.map(
    async (id) =>
      await get(child(dbRef, `${collection}/${id}`)).then((resultDB) =>
        resultDB.val()
      )
  );
  return await Promise.all(result).then((data) => {
    let result = [];
    data.map((r) => {
      return result.push({ name: r.name, id: r.id });
    });
    return result;
  });
};

export const addSpecialty = async (cluster, specialty) => {
  await firebase
    .database()
    .ref("clusters/" + cluster + "/specialties/" + specialty + "/")
    .set(specialty);
  return "ok";
};
